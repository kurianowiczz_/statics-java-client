import { Client } from './client';
import { CookieNotProvidedException } from './errors';

export const createJavaClient = (cookie: string) => {
  if (!cookie) {
    throw new CookieNotProvidedException();
  }

  const instance = new Client(cookie);

  return instance.init();
};
