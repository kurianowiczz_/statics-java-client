export class BadGatewayException extends Error {
  code = 502;
  message = 'Bad Gateway';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
