export class CookieNotProvidedException extends Error {
  message = 'You should provide cookies in order to use this client.';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
