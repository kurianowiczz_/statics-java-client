export class NotFoundException extends Error {
  code = 404;
  message = 'Not found';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
