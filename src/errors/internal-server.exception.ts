export class InternalServerException extends Error {
  code = 500;
  message = 'Internal server error';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
