import {
  BadGatewayException,
  BadRequestException,
  ConflictException,
  CookieNotProvidedException,
  ForbiddenException,
  GatewayTimeoutException,
  InternalServerException,
  MethodNotAllowedException,
  NotFoundException,
  ServiceUnavailableException,
  UnauthorizedException,
} from './index';

export abstract class ErrorFactory {
  static create(statusCode: number, message?: string) {
    if (statusCode === 400) {
      throw new BadRequestException(message);
    } else if (statusCode === 401) {
      throw new UnauthorizedException(message);
    } else if (statusCode === 403) {
      throw new ForbiddenException(message);
    } else if (statusCode === 404) {
      throw new NotFoundException(message);
    } else if (statusCode === 405) {
      throw new MethodNotAllowedException(message);
    } else if (statusCode === 409) {
      throw new ConflictException(message);
    } else if (statusCode === 500) {
      throw new InternalServerException(message);
    } else if (statusCode === 502) {
      throw new BadGatewayException(message);
    } else if (statusCode === 503) {
      throw new ServiceUnavailableException(message);
    } else if (statusCode === 504) {
      throw new GatewayTimeoutException(message);
    } else {
      throw new CookieNotProvidedException(message);
    }
  }
}
