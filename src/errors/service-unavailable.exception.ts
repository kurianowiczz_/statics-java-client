export class ServiceUnavailableException extends Error {
  code = 503;
  message = 'Service Unavailable';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
