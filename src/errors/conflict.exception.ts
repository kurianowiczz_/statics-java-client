export class ConflictException extends Error {
  code = 409;
  message = 'Conflict';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
