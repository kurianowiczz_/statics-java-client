export class MethodNotAllowedException extends Error {
  code = 405;
  message = 'Method Not Allowed';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
