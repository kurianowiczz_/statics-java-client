export class GatewayTimeoutException extends Error {
  code = 504;
  message = 'Internal server error';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
