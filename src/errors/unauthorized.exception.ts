export class UnauthorizedException extends Error {
  code = 401;
  message = 'Unauthorized';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
