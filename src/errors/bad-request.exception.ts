export class BadRequestException extends Error {
  code = 400;
  message = 'Bad Request';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
