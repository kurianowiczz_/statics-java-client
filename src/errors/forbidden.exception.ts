export class ForbiddenException extends Error {
  code = 403;
  message = 'Forbidden';

  constructor(message?: string) {
    super();
    this.message = message ?? this.message;
  }
}
