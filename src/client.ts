import { Agent } from 'https';
import axios, { AxiosInstance, AxiosRequestHeaders } from 'axios';
import * as CONSTANTS from './common/constants';

import { replacePlaceholders } from './common/utils';
import { ErrorFactory } from './errors/error-factory';
import { JavaClient } from './java-client.interface';
import routes from './routes';

export class Client implements JavaClient {
  private readonly cookie: string;
  private client: AxiosInstance;
  private readonly headers: AxiosRequestHeaders = {
    'Content-Type': 'application/json',
  };

  constructor(cookie: string) {
    this.cookie = cookie;
  }

  init(): JavaClient {
    this.client = axios.create({
      baseURL: CONSTANTS.JAVA_CLIENT_URL,
      httpsAgent: new Agent({ rejectUnauthorized: false }),
      headers: { ...this.headers, JSESSIONID: this.cookie },
    });

    return this;
  }

  // TODO: create a return type
  async getPersons() {
    try {
      return await this.client.get(routes.GET_PERSONS_LIST);
    } catch (error) {
      this.handleError(error);
    }
  }

  // TODO: create a return type
  async getPersonDetails(account: string) {
    try {
      return await this.client.get(
        replacePlaceholders(routes.GET_PERSON_DETAILS, { account }),
      );
    } catch (error) {
      this.handleError(error);
    }
  }

  // TODO: create a return type
  async getAuth() {
    try {
      return await this.client.get(routes.GET_AUTH);
    } catch (error) {
      this.handleError(error);
    }
  }

  // TODO: create a return type
  async getPersonTags(account: string) {
    try {
      return await this.client.get(
        replacePlaceholders(routes.GET_PERSON_TAGS, { account }),
      );
    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError(err): never {
    const { message, statusCode, response } = err;
    const code = statusCode || response?.status;
    throw ErrorFactory.create(code, message);
  }
}
