export const replacePlaceholders = (
  str: string,
  placeholders: Record<string, string | number>,
): string => {
  const parts = str.split(/(\{\{\w+?}})/g).map((v) => {
    const replaced = v.replace(/\{\{(\w+?)}}/g, '$1');
    return placeholders[replaced] || v;
  });

  return parts.join('');
};
