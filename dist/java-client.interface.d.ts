export interface JavaClient {
    init(): JavaClient;
    getPersons(): any;
    getPersonDetails(account: string): any;
    getAuth(): any;
    getPersonTags(account: string): any;
}
