import { JavaClient } from './java-client.interface';
export declare class Client implements JavaClient {
    private readonly cookie;
    private client;
    private readonly headers;
    constructor(cookie: string);
    init(): JavaClient;
    getPersons(): Promise<import("axios").AxiosResponse<any, any>>;
    getPersonDetails(account: string): Promise<import("axios").AxiosResponse<any, any>>;
    getAuth(): Promise<import("axios").AxiosResponse<any, any>>;
    getPersonTags(account: string): Promise<import("axios").AxiosResponse<any, any>>;
    private handleError;
}
