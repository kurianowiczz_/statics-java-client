"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Client = void 0;
const https_1 = require("https");
const axios_1 = require("axios");
const CONSTANTS = require("./common/constants");
const utils_1 = require("./common/utils");
const error_factory_1 = require("./errors/error-factory");
const routes_1 = require("./routes");
class Client {
    constructor(cookie) {
        this.headers = {
            'Content-Type': 'application/json',
        };
        this.cookie = cookie;
    }
    init() {
        this.client = axios_1.default.create({
            baseURL: CONSTANTS.JAVA_CLIENT_URL,
            httpsAgent: new https_1.Agent({ rejectUnauthorized: false }),
            headers: Object.assign(Object.assign({}, this.headers), { _JSESSIONID: this.cookie }),
        });
        return this;
    }
    async getPersons() {
        try {
            return await this.client.get(routes_1.default.GET_PERSONS_LIST);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    async getPersonDetails(account) {
        try {
            return await this.client.get((0, utils_1.replacePlaceholders)(routes_1.default.GET_PERSON_DETAILS, { account }));
        }
        catch (error) {
            this.handleError(error);
        }
    }
    async getAuth() {
        try {
            return await this.client.get(routes_1.default.GET_AUTH);
        }
        catch (error) {
            this.handleError(error);
        }
    }
    async getPersonTags(account) {
        try {
            return await this.client.get((0, utils_1.replacePlaceholders)(routes_1.default.GET_PERSON_TAGS, { account }));
        }
        catch (error) {
            this.handleError(error);
        }
    }
    handleError(err) {
        const { message, statusCode, response } = err;
        const code = statusCode || (response === null || response === void 0 ? void 0 : response.status);
        throw error_factory_1.ErrorFactory.create(code, message);
    }
}
exports.Client = Client;
//# sourceMappingURL=client.js.map