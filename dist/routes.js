"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    GET_PERSONS_LIST: 'persons',
    GET_PERSON_DETAILS: 'persons/{{account}}',
    GET_AUTH: 'auth',
    GET_PERSON_TAGS: 'persons/{{account}}/tags',
};
//# sourceMappingURL=routes.js.map