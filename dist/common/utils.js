"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.replacePlaceholders = void 0;
const replacePlaceholders = (str, placeholders) => {
    const parts = str.split(/(\{\{\w+?}})/g).map((v) => {
        const replaced = v.replace(/\{\{(\w+?)}}/g, '$1');
        return placeholders[replaced] || v;
    });
    return parts.join('');
};
exports.replacePlaceholders = replacePlaceholders;
//# sourceMappingURL=utils.js.map