export declare const replacePlaceholders: (str: string, placeholders: Record<string, string | number>) => string;
