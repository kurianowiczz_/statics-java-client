"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InternalServerException = void 0;
class InternalServerException extends Error {
    constructor(message) {
        super();
        this.code = 500;
        this.message = 'Internal server error';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.InternalServerException = InternalServerException;
//# sourceMappingURL=internal-server.exception.js.map