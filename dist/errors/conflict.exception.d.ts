export declare class ConflictException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
