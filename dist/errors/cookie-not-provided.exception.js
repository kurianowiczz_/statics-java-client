"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CookieNotProvidedException = void 0;
class CookieNotProvidedException extends Error {
    constructor(message) {
        super();
        this.message = 'You should provide cookies in order to use this client.';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.CookieNotProvidedException = CookieNotProvidedException;
//# sourceMappingURL=cookie-not-provided.exception.js.map