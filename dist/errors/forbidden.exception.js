"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForbiddenException = void 0;
class ForbiddenException extends Error {
    constructor(message) {
        super();
        this.code = 403;
        this.message = 'Forbidden';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.ForbiddenException = ForbiddenException;
//# sourceMappingURL=forbidden.exception.js.map