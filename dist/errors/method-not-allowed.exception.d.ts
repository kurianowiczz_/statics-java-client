export declare class MethodNotAllowedException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
