export declare class CookieNotProvidedException extends Error {
    message: string;
    constructor(message?: string);
}
