export declare class NotFoundException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
