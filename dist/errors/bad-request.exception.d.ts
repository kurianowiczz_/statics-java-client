export declare class BadRequestException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
