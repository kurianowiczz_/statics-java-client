export declare class InternalServerException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
