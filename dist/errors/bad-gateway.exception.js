"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadGatewayException = void 0;
class BadGatewayException extends Error {
    constructor(message) {
        super();
        this.code = 502;
        this.message = 'Bad Gateway';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.BadGatewayException = BadGatewayException;
//# sourceMappingURL=bad-gateway.exception.js.map