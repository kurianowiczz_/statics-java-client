export declare class BadGatewayException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
