export declare class GatewayTimeoutException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
