"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorFactory = void 0;
const index_1 = require("./index");
class ErrorFactory {
    static create(statusCode, message) {
        if (statusCode === 400) {
            throw new index_1.BadRequestException(message);
        }
        else if (statusCode === 401) {
            throw new index_1.UnauthorizedException(message);
        }
        else if (statusCode === 403) {
            throw new index_1.ForbiddenException(message);
        }
        else if (statusCode === 404) {
            throw new index_1.NotFoundException(message);
        }
        else if (statusCode === 405) {
            throw new index_1.MethodNotAllowedException(message);
        }
        else if (statusCode === 409) {
            throw new index_1.ConflictException(message);
        }
        else if (statusCode === 500) {
            throw new index_1.InternalServerException(message);
        }
        else if (statusCode === 502) {
            throw new index_1.BadGatewayException(message);
        }
        else if (statusCode === 503) {
            throw new index_1.ServiceUnavailableException(message);
        }
        else if (statusCode === 504) {
            throw new index_1.GatewayTimeoutException(message);
        }
        else {
            throw new index_1.CookieNotProvidedException(message);
        }
    }
}
exports.ErrorFactory = ErrorFactory;
//# sourceMappingURL=error-factory.js.map