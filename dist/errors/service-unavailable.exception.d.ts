export declare class ServiceUnavailableException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
