export declare abstract class ErrorFactory {
    static create(statusCode: number, message?: string): void;
}
