"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodNotAllowedException = void 0;
class MethodNotAllowedException extends Error {
    constructor(message) {
        super();
        this.code = 405;
        this.message = 'Method Not Allowed';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.MethodNotAllowedException = MethodNotAllowedException;
//# sourceMappingURL=method-not-allowed.exception.js.map