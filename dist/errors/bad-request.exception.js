"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BadRequestException = void 0;
class BadRequestException extends Error {
    constructor(message) {
        super();
        this.code = 400;
        this.message = 'Bad Request';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.BadRequestException = BadRequestException;
//# sourceMappingURL=bad-request.exception.js.map