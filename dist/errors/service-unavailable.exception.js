"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceUnavailableException = void 0;
class ServiceUnavailableException extends Error {
    constructor(message) {
        super();
        this.code = 503;
        this.message = 'Service Unavailable';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.ServiceUnavailableException = ServiceUnavailableException;
//# sourceMappingURL=service-unavailable.exception.js.map