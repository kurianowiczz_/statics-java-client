export declare class UnauthorizedException extends Error {
    code: number;
    message: string;
    constructor(message?: string);
}
