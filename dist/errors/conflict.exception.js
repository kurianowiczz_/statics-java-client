"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConflictException = void 0;
class ConflictException extends Error {
    constructor(message) {
        super();
        this.code = 409;
        this.message = 'Conflict';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.ConflictException = ConflictException;
//# sourceMappingURL=conflict.exception.js.map