"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnauthorizedException = void 0;
class UnauthorizedException extends Error {
    constructor(message) {
        super();
        this.code = 401;
        this.message = 'Unauthorized';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.UnauthorizedException = UnauthorizedException;
//# sourceMappingURL=unauthorized.exception.js.map