"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GatewayTimeoutException = void 0;
class GatewayTimeoutException extends Error {
    constructor(message) {
        super();
        this.code = 504;
        this.message = 'Internal server error';
        this.message = message !== null && message !== void 0 ? message : this.message;
    }
}
exports.GatewayTimeoutException = GatewayTimeoutException;
//# sourceMappingURL=gateway-timeout.exception.js.map