"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createJavaClient = void 0;
const client_1 = require("./client");
const errors_1 = require("./errors");
const createJavaClient = (cookie) => {
    if (!cookie) {
        throw new errors_1.CookieNotProvidedException();
    }
    const instance = new client_1.Client(cookie);
    return instance.init();
};
exports.createJavaClient = createJavaClient;
//# sourceMappingURL=java-client.factory.js.map