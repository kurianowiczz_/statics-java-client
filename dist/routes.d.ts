declare const _default: {
    GET_PERSONS_LIST: string;
    GET_PERSON_DETAILS: string;
    GET_AUTH: string;
    GET_PERSON_TAGS: string;
};
export default _default;
